//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file plm-uniform.cpp
//  \brief  piecewise linear reconstruction for both uniform and non-uniform meshes

// Athena++ headers
#include "reconstruction.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../hydro/hydro.hpp"
#include "../mesh/mesh.hpp"
#include "../coordinates/coordinates.hpp"
#include "../eos/eos.hpp"


//----------------------------------------------------------------------------------------
//! \fn Reconstruction::PiecewiseLinearX1()
//  \brief

void Reconstruction::PiecewiseLinearX1(MeshBlock *pmb,
  const int kl, const int ku, const int jl, const int ju, const int il, const int iu,
  const AthenaArray<Real> &w_in, const AthenaArray<Real> &bcc_in,
  AthenaArray<Real> &wl_in, AthenaArray<Real> &wr_in) {

  if(COORDINATE_SYSTEM != "cartesian" || 
      pmb->precon->characteristic_reconstruction) {
    throw std::runtime_error(
        "PLM reconstruction doesn't support non-cartesian grids, "
        "nor characteristic reconstruction");
  }

  auto w = w_in.get_KView4D();
  auto wl = wl_in.get_KView4D();
  auto wr = wr_in.get_KView4D();
#if MAGNETIC_FIELDS_ENABLED
  auto bcc = bcc_in.get_KView4D();
#endif

  //Grab some variables for the KOKKOS_LAMBDA to get from context
  bool uniform_limiter = pmb->precon->uniform_limiter[X1DIR];

  athena_for("PLM X1",kl,ku,jl,ju,il-1,iu,
  KOKKOS_LAMBDA (int k, int j, int i) {
      
      Real dwl[NWAVE], dwr[NWAVE], wc[NWAVE]; //NWAVE==NHYDRO for non-mhd?
      for (int n=0; n<(NHYDRO); ++n) {
        dwl[n] = (w(n,k,j,i  ) - w(n,k,j,i-1));
        dwr[n] = (w(n,k,j,i+1) - w(n,k,j,i  ));
        wc[n] = w(n,k,j,i);
      }
#if MAGNETIC_FIELDS_ENABLED
        //bx(i) = bcc(IB1,k,j,i);

        dwl[IBY] = (bcc(IB2,k,j,i  ) - bcc(IB2,k,j,i-1));
        dwr[IBY] = (bcc(IB2,k,j,i+1) - bcc(IB2,k,j,i  ));
        wc[IBY] = bcc(IB2,k,j,i);

        dwl[IBZ] = (bcc(IB3,k,j,i  ) - bcc(IB3,k,j,i-1));
        dwr[IBZ] = (bcc(IB3,k,j,i+1) - bcc(IB3,k,j,i  ));
        wc[IBZ] = bcc(IB3,k,j,i);
#endif

      // Project slopes to characteristic variables, if necessary
      // Note order of characteristic fields in output vect corresponds to (IVX,IVY,IVZ)
      /*if (characteristic_reconstruction) {
        LeftEigenmatrixDotVector(pmb,IVX,il-1,iu,bx,wc,dwl);
        LeftEigenmatrixDotVector(pmb,IVX,il-1,iu,bx,wc,dwr);
      }*/

      Real dwm[NWAVE];
      // Apply van Leer limiter for uniform grid
      if (uniform_limiter) {
        Real dw2;
        for (int n=0; n<(NWAVE); ++n) {
          dw2 = dwl[n]*dwr[n];
          dwm[n] = 2.0*dw2/(dwl[n] + dwr[n]);
          if (dw2 <= 0.0) dwm[n] = 0.0;
        }

      // Apply Mignone limiter for non-uniform grid
      }
#if 0
      else { //For non-cartesian grids // NOLINT
        for (int n=0; n<(NWAVE); ++n) {
          dw2(i) = dwl(n,i)*dwr(n,i);
          Real cf = pco->dx1v(i  )/(pco->x1f(i+1) - pco->x1v(i));
          Real cb = pco->dx1v(i-1)/(pco->x1v(i  ) - pco->x1f(i));
          dwm[n] = (dw2*(cf*dwl[n] + cb*dwr[n])/
            (SQR(dwl[n]) + SQR(dwr[n]) + dw2*(cf + cb - 2.0)));
          if (dw2 <= 0.0) dwm[n] = 0.0;
        }
      }
#endif
      // Project limited slope back to primitive variables, if necessary
      /*if (characteristic_reconstruction) {
        RightEigenmatrixDotVector(pmb,IVX,il-1,iu,bx,wc,dwm);
      }*/

      // compute ql_(i+1/2) and qr_(i-1/2) using monotonized slopes
      for (int n=0; n<(NWAVE); ++n) {
          wl(n,k,j,i+1) = wc[n] + 0.5*dwm[n];
          wr(n,k,j,i  ) = wc[n] - 0.5*dwm[n];
          /*if (characteristic_reconstruction) {
            // Reapply EOS floors to both L/R reconstructed primitive states
            pmb->peos->ApplyPrimitiveFloors(wl, k, j, i+1);
            pmb->peos->ApplyPrimitiveFloors(wr, k, j, i);
        }*/
      }
  
  });

  return;
}

//----------------------------------------------------------------------------------------
//! \fn Reconstruction::PiecewiseLinearX2()
//  \brief

void Reconstruction::PiecewiseLinearX2(MeshBlock *pmb,
  const int kl, const int ku, const int jl, const int ju, const int il, const int iu,
  const AthenaArray<Real> &w_in, const AthenaArray<Real> &bcc_in,
  AthenaArray<Real> &wl_in, AthenaArray<Real> &wr_in) {

  if(COORDINATE_SYSTEM != "cartesian" || 
      pmb->precon->characteristic_reconstruction) {
    throw std::runtime_error(
        "PLM reconstruction doesn't support non-cartesian grids, "
        "nor characteristic reconstruction");
  }

  auto w = w_in.get_KView4D();
  auto wl = wl_in.get_KView4D();
  auto wr = wr_in.get_KView4D();
#if MAGNETIC_FIELDS_ENABLED
  auto bcc = bcc_in.get_KView4D();
#endif

  //Grab some variables for the KOKKOS_LAMBDA to get from context
  bool uniform_limiter = pmb->precon->uniform_limiter[X2DIR];

  athena_for("PLM X2",kl,ku,jl-1,ju,il,iu,
  KOKKOS_LAMBDA (int k, int j, int i) {
      Real dwl[NWAVE], dwr[NWAVE], wc[NWAVE]; //NWAVE==NHYDRO for non-mhd?
      for (int n=0; n<(NHYDRO); ++n) {
        dwl[n] = (w(n,k,j  ,i) - w(n,k,j-1,i));
        dwr[n] = (w(n,k,j+1,i) - w(n,k,j  ,i));
        wc[n] = w(n,k,j,i);
      }

#if MAGNETIC_FIELDS_ENABLED
        //bx(i) = bcc(IB2,k,j,i);

        dwl[IBY] = (bcc(IB3,k,j  ,i) - bcc(IB3,k,j-1,i));
        dwr[IBY] = (bcc(IB3,k,j+1,i) - bcc(IB3,k,j  ,i));
        wc[IBY] = bcc(IB3,k,j,i);

        dwl[IBZ] = (bcc(IB1,k,j  ,i) - bcc(IB1,k,j-1,i));
        dwr[IBZ] = (bcc(IB1,k,j+1,i) - bcc(IB1,k,j  ,i));
        wc[IBZ] = bcc(IB1,k,j,i);
#endif

      // Project slopes to characteristic variables, if necessary
      // Note order of characteristic fields in output vect corresponds to (IVY,IVZ,IVX)
      /*if (pmb->precon->characteristic_reconstruction) {
        LeftEigenmatrixDotVector(pmb,IVY,il,iu,bx,wc,dwl);
        LeftEigenmatrixDotVector(pmb,IVY,il,iu,bx,wc,dwr);
      }*/
      
      Real dwm[NWAVE];
      // Apply van Leer limiter for uniform grid
      if (uniform_limiter) {
        Real dw2;
        for (int n=0; n<(NWAVE); ++n) {
          dw2 = dwl[n]*dwr[n];
          dwm[n] = 2.0*dw2/(dwl[n] + dwr[n]);
          if (dw2 <= 0.0) dwm[n] = 0.0;
        }

      // Apply Mignone limiter for non-uniform grid
      }
#if 0
      else { //For non-cartesian grids // NOLINT
        for (int n=0; n<(NWAVE); ++n) {
          dw2(i) = dwl(n,i)*dwr(n,i);
          Real cf = pco->dx2v(j  )/(pco->x2f(j+1) - pco->x2v(j));
          Real cb = pco->dx2v(j-1)/(pco->x2v(j  ) - pco->x2f(j));
          dwm(n,i) = (dw2(i)*(cf*dwl(n,i) + cb*dwr(n,i))/
            (SQR(dwl(n,i)) + SQR(dwr(n,i)) + dw2(i)*(cf + cb - 2.0)));
          if (dw2(i) <= 0.0) dwm(n,i) = 0.0;
        }
      }
#endif

      // Project limited slope back to primitive variables, if necessary
      /*if (pmb->precon->characteristic_reconstruction) {
        RightEigenmatrixDotVector(pmb,IVY,il,iu,bx,wc,dwm);
      }*/

      // compute ql_(j+1/2) and qr_(j-1/2) using monotonized slopes
      for (int n=0; n<(NWAVE); ++n) {
        wl(n,k,j+1,i) = wc[n] + 0.5*dwm[n];
        wr(n,k,j  ,i) = wc[n] - 0.5*dwm[n];
        /*if (pmb->precon->characteristic_reconstruction) {
          // Reapply EOS floors to both L/R reconstructed primitive states
          pmb->peos->ApplyPrimitiveFloors(wl, k, j+1, i);
          pmb->peos->ApplyPrimitiveFloors(wr, k, j, i);
        }*/
      }
  });

  return;
}

//----------------------------------------------------------------------------------------
//! \fn Reconstruction::PiecewiseLinearX3()
//  \brief

void Reconstruction::PiecewiseLinearX3(MeshBlock *pmb,
  const int kl, const int ku, const int jl, const int ju, const int il, const int iu,
  const AthenaArray<Real> &w_in, const AthenaArray<Real> &bcc_in,
  AthenaArray<Real> &wl_in, AthenaArray<Real> &wr_in) {

  if(COORDINATE_SYSTEM != "cartesian" || 
      pmb->precon->characteristic_reconstruction) {
    throw std::runtime_error(
        "PLM reconstruction doesn't support non-cartesian grids, "
        "nor characteristic reconstruction");
  }

  auto w = w_in.get_KView4D();
  auto wl = wl_in.get_KView4D();
  auto wr = wr_in.get_KView4D();
#if MAGNETIC_FIELDS_ENABLED
  auto bcc = bcc_in.get_KView4D();
#endif

  //Grab some variables for the KOKKOS_LAMBDA to get from context
  bool uniform_limiter = pmb->precon->uniform_limiter[X3DIR];

  athena_for("PLM X3",kl-1,ku,jl,ju,il,iu,
  KOKKOS_LAMBDA (int k, int j, int i) {
      Real dwl[NWAVE], dwr[NWAVE], wc[NWAVE]; //NWAVE==NHYDRO for non-mhd?
      for (int n=0; n<(NHYDRO); ++n) {
        dwl[n] = (w(n,k  ,j,i) - w(n,k-1,j,i));
        dwr[n] = (w(n,k+1,j,i) - w(n,k  ,j,i));
        wc[n] = w(n,k,j,i);
      }

#if MAGNETIC_FIELDS_ENABLED
        //bx(i) = bcc(IB3,k,j,i);

        dwl[IBY] = (bcc(IB1,k  ,j,i) - bcc(IB1,k-1,j,i));
        dwr[IBY] = (bcc(IB1,k+1,j,i) - bcc(IB1,k  ,j,i));
        wc[IBY] = bcc(IB1,k,j,i);

        dwl[IBZ] = (bcc(IB2,k  ,j,i) - bcc(IB2,k-1,j,i));
        dwr[IBZ] = (bcc(IB2,k+1,j,i) - bcc(IB2,k  ,j,i));
        wc[IBZ] = bcc(IB2,k,j,i);
#endif

      // Project slopes to characteristic variables, if necessary
      // Note order of characteristic fields in output vect corresponds to (IVZ,IVX,IVY)
      /*if (pmb->precon->characteristic_reconstruction) {
        LeftEigenmatrixDotVector(pmb,IVZ,il,iu,bx,wc,dwl);
        LeftEigenmatrixDotVector(pmb,IVZ,il,iu,bx,wc,dwr);
      }*/

      Real dwm[NWAVE];
      // Apply van Leer limiter for uniform grid
      if (uniform_limiter) {
        Real dw2;
        for (int n=0; n<(NWAVE); ++n) {
          dw2 = dwl[n]*dwr[n];
          dwm[n] = 2.0*dw2/(dwl[n] + dwr[n]);
          if (dw2 <= 0.0) dwm[n] = 0.0;
        }

      // Apply Mignone limiter for non-uniform grid
      }
#if 0
      else { //For non-cartesian grids // NOLINT
        for (int n=0; n<(NWAVE); ++n) {
          dw2(i) = dwl(n,i)*dwr(n,i);
          Real cf = pco->dx3v(k  )/(pco->x3f(k+1) - pco->x3v(k));
          Real cb = pco->dx3v(k-1)/(pco->x3v(k  ) - pco->x3f(k));
          dwm(n,i) = (dw2(i)*(cf*dwl(n,i) + cb*dwr(n,i))/
            (SQR(dwl(n,i)) + SQR(dwr(n,i)) + dw2(i)*(cf + cb - 2.0)));
          if (dw2(i) <= 0.0) dwm(n,i) = 0.0;
        }
      }
#endif

      // Project limited slope back to primitive variables, if necessary
      /*if (pmb->precon->characteristic_reconstruction) {
        RightEigenmatrixDotVector(pmb,IVZ,il,iu,bx,wc,dwm);
      }*/

      // compute ql_(k+1/2) and qr_(k-1/2) using monotonized slopes
      for (int n=0; n<(NWAVE); ++n) {
        wl(n,k+1,j,i) = wc[n] + 0.5*dwm[n];
        wr(n,k  ,j,i) = wc[n] - 0.5*dwm[n];
        /*if (pmb->precon->characteristic_reconstruction) {
          // Reapply EOS floors to both L/R reconstructed primitive states
          pmb->peos->ApplyPrimitiveFloors(wl, k+1, j, i);
          pmb->peos->ApplyPrimitiveFloors(wr, k, j, i);
        }*/
      }
  });

  return;
}
