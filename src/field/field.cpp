//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file field.cpp
//  \brief implementation of functions in class Field

// C++ headers
#include <string>

// Athena++ headers
#include "field.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../coordinates/coordinates.hpp"
#include "field_diffusion/field_diffusion.hpp"
#include "../mesh/mesh.hpp"
#include "../reconstruct/reconstruction.hpp"

// constructor, initializes data structures and parameters

Field::Field(MeshBlock *pmb, ParameterInput *pin) {
  pmy_block = pmb;

  // Allocate memory for interface fields, but only when needed.
  if (MAGNETIC_FIELDS_ENABLED) {
    int ncells1 = pmb->block_size.nx1 + 2*(NGHOST);
    int ncells2 = 1, ncells3 = 1;
    if (pmb->block_size.nx2 > 1) ncells2 = pmb->block_size.nx2 + 2*(NGHOST);
    if (pmb->block_size.nx3 > 1) ncells3 = pmb->block_size.nx3 + 2*(NGHOST);

    //  Note the extra cell in each longitudinal dirn for interface fields
    b.x1f.NewAthenaArray( ncells3   , ncells2   ,(ncells1+1));
    b.x2f.NewAthenaArray( ncells3   ,(ncells2+1), ncells1   );
    b.x3f.NewAthenaArray((ncells3+1), ncells2   , ncells1   );

    b1.x1f.NewAthenaArray( ncells3   , ncells2   ,(ncells1+1));
    b1.x2f.NewAthenaArray( ncells3   ,(ncells2+1), ncells1   );
    b1.x3f.NewAthenaArray((ncells3+1), ncells2   , ncells1   );
    // If user-requested time integrator is type 3S*, allocate additional memory registers
    std::string integrator = pin->GetOrAddString("time","integrator","vl2");
    if (integrator == "ssprk5_4") {
      // future extension may add "int nregister" to Hydro class
      b2.x1f.NewAthenaArray( ncells3   , ncells2   ,(ncells1+1));
      b2.x2f.NewAthenaArray( ncells3   ,(ncells2+1), ncells1   );
      b2.x3f.NewAthenaArray((ncells3+1), ncells2   , ncells1   );
    }

    bcc.NewAthenaArray (NFIELD,ncells3,ncells2,ncells1);

    e.x1e.NewAthenaArray((ncells3+1),(ncells2+1), ncells1   );
    e.x2e.NewAthenaArray((ncells3+1), ncells2   ,(ncells1+1));
    e.x3e.NewAthenaArray( ncells3   ,(ncells2+1),(ncells1+1));

    wght.x1f.NewAthenaArray( ncells3   , ncells2   ,(ncells1+1));
    wght.x2f.NewAthenaArray( ncells3   ,(ncells2+1), ncells1   );
    wght.x3f.NewAthenaArray((ncells3+1), ncells2   , ncells1   );

    e2_x1f_.NewAthenaArray( ncells3   , ncells2   ,(ncells1+1));
    e3_x1f_.NewAthenaArray( ncells3   , ncells2   ,(ncells1+1));
    e1_x2f_.NewAthenaArray( ncells3   ,(ncells2+1), ncells1   );
    e3_x2f_.NewAthenaArray( ncells3   ,(ncells2+1), ncells1   );
    e1_x3f_.NewAthenaArray((ncells3+1), ncells2   , ncells1   );
    e2_x3f_.NewAthenaArray((ncells3+1), ncells2   , ncells1   );

    // Allocate memory for scratch vectors
    cc_e.NewAthenaArray(ncells3,ncells2,ncells1);

    face_area_.NewAthenaArray(ncells1);
    edge_length_.NewAthenaArray(ncells1);
    edge_length_p1_.NewAthenaArray(ncells1);
    if (GENERAL_RELATIVITY) {
      g_.NewAthenaArray(NMETRIC,ncells1);
      gi_.NewAthenaArray(NMETRIC,ncells1);
    }
// ptr to diffusion object
    pfdif = new FieldDiffusion(pmb,pin);
  }
}

// destructor

Field::~Field() {
  b.x1f.DeleteAthenaArray();
  b.x2f.DeleteAthenaArray();
  b.x3f.DeleteAthenaArray();
  b1.x1f.DeleteAthenaArray();
  b1.x2f.DeleteAthenaArray();
  b1.x3f.DeleteAthenaArray();
  // b2 only allocated if integrator was 3S* integrator
  b2.x1f.DeleteAthenaArray();
  b2.x2f.DeleteAthenaArray();
  b2.x3f.DeleteAthenaArray();
  bcc.DeleteAthenaArray();

  e.x1e.DeleteAthenaArray();
  e.x2e.DeleteAthenaArray();
  e.x3e.DeleteAthenaArray();
  wght.x1f.DeleteAthenaArray();
  wght.x2f.DeleteAthenaArray();
  wght.x3f.DeleteAthenaArray();
  e2_x1f_.DeleteAthenaArray();
  e3_x1f_.DeleteAthenaArray();
  e1_x2f_.DeleteAthenaArray();
  e3_x2f_.DeleteAthenaArray();
  e1_x3f_.DeleteAthenaArray();
  e2_x3f_.DeleteAthenaArray();

  cc_e.DeleteAthenaArray();
  face_area_.DeleteAthenaArray();
  edge_length_.DeleteAthenaArray();
  edge_length_p1_.DeleteAthenaArray();
  if (GENERAL_RELATIVITY) {
    g_.DeleteAthenaArray();
    gi_.DeleteAthenaArray();
  }
  delete pfdif;
}

//----------------------------------------------------------------------------------------
// \! fn
// \! brief

void Field::CalculateCellCenteredField(const FaceField &bf, AthenaArray<Real> &bc_in,
            Coordinates *pco, int is, int ie, int js, int je, int ks, int ke) {
  // Defer to Reconstruction class to check if uniform Cartesian formula can be used
  // (unweighted average)
  const bool uniform_ave_x1 = pmy_block->precon->uniform_limiter[0];
  const bool uniform_ave_x2 = pmy_block->precon->uniform_limiter[1];
  const bool uniform_ave_x3 = pmy_block->precon->uniform_limiter[2];

  auto bc = bc_in.get_KView4D();

  auto bf_x1f = bf.x1f.get_KView3D();
  auto bf_x2f = bf.x2f.get_KView3D();
  auto bf_x3f = bf.x3f.get_KView3D();

  // splitting the kernel from the original single kernel as it caused problem with nvcc
  // see https://gitlab.com/pgrete/kathena/issues/45
  if (uniform_ave_x1 and uniform_ave_x2 and uniform_ave_x3) {
    athena_for("CalculateCellCenteredField",ks,ke,js,je,is,ie,
    KOKKOS_LAMBDA (int k, int j, int i) {
      bc(IB1,k,j,i) = 0.5 * (bf_x1f(k,j,i  ) + bf_x1f(k,j,i+1));
      bc(IB2,k,j,i) = 0.5 * (bf_x2f(k,j  ,i) + bf_x2f(k,j+1,i));
      bc(IB3,k,j,i) = 0.5 * (bf_x3f(k  ,j,i) + bf_x3f(k+1,j,i));
    });
  } else {
    // cell center B-fields are defined as spatial interpolation at the volume center
    if (uniform_ave_x1) {
      athena_for("CalculateCellCenteredField x1",ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int k, int j, int i) {
        bc(IB1,k,j,i) = 0.5 * (bf_x1f(k,j,i  ) + bf_x1f(k,j,i+1));
      });

    } else {
      auto x1f  = pco->x1f.get_KView1D();
      auto x1v  = pco->x1v.get_KView1D();
      auto dx1f = pco->dx1f.get_KView1D();

      athena_for("CalculateCellCenteredField x1",ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int k, int j, int i) {
        // linear interpolation coefficients from lower and upper cell faces
        const Real lw = (x1f(i+1) - x1v(i))/dx1f(i);
        const Real rw = (x1v(i)   - x1f(i))/dx1f(i);

        bc(IB1,k,j,i) = lw*bf_x1f(k,j,i  ) + rw*bf_x1f(k,j,i+1);
      });
    }

    if (uniform_ave_x2) {

      athena_for("CalculateCellCenteredField x2",ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int k, int j, int i) {
        bc(IB2,k,j,i) = 0.5 * (bf_x2f(k,j  ,i) + bf_x2f(k,j+1,i));
      });

    } else {
      auto x2f  = pco->x2f.get_KView1D();
      auto x2v  = pco->x2v.get_KView1D();
      auto dx2f = pco->dx2f.get_KView1D();

      athena_for("CalculateCellCenteredField x2",ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int k, int j, int i) {
        const Real lw = (x2f(j+1) - x2v(j))/dx2f(j);
        const Real rw = (x2v(j)   - x2f(j))/dx2f(j);

        bc(IB2,k,j,i) = lw*bf_x2f(k,j  ,i) +rw* bf_x2f(k,j+1,i);
      });
    }
    if (uniform_ave_x3) {

      athena_for("CalculateCellCenteredField x3",ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int k, int j, int i) {
        bc(IB3,k,j,i) = 0.5 * (bf_x3f(k  ,j,i) + bf_x3f(k+1,j,i));
      });

    } else {
      auto x3f  = pco->x3f.get_KView1D();
      auto x3v  = pco->x3v.get_KView1D();
      auto dx3f = pco->dx3f.get_KView1D();

      athena_for("CalculateCellCenteredField x3",ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int k, int j, int i) {
        const Real lw = (x3f(k+1) - x3v(k))/dx3f(k);
        const Real rw = (x3v(k)   - x3f(k))/dx3f(k);

        bc(IB3,k,j,i) = lw*bf_x3f(k  ,j,i) + rw*bf_x3f(k+1,j,i);
      });
    }
  }

  return;
}
