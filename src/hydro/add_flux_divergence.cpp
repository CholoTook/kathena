//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file add_flux_divergence.cpp
//  \brief Applies divergence of the fluxes, including geometric "source terms" added
//         by a function implemented in each Coordinate class.

// Athena++ headers
#include "hydro.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../coordinates/coordinates.hpp"
#include "../field/field.hpp"
#include "../mesh/mesh.hpp"
#include "../bvals/bvals.hpp"
#include "../reconstruct/reconstruction.hpp"

// OpenMP header
#ifdef OPENMP_PARALLEL
#include <omp.h>
#endif

//----------------------------------------------------------------------------------------
//! \fn  void Hydro::AddFluxDivergenceToAverage
//  \brief Adds flux divergence to weighted average of conservative variables from
//  previous step(s) of time integrator algorithm

void Hydro::AddFluxDivergenceToAverage(AthenaArray<Real> &w_in, AthenaArray<Real> &bcc,
                                       const Real wght, AthenaArray<Real> &u_out_in) {
  if(COORDINATE_SYSTEM != "cartesian") {
    throw std::runtime_error(
        "AddFluxDivergenceToAverage doesn't support non-cartesian grids"
        "(nor non-uniform blocks).");
  }

  MeshBlock *pmb=pmy_block;
  AthenaArray<Real> &x1flux_in=flux[X1DIR];
  AthenaArray<Real> &x2flux_in=flux[X2DIR];
  AthenaArray<Real> &x3flux_in=flux[X3DIR];

  auto x1flux = x1flux_in.get_KView4D();
  auto x2flux = x2flux_in.get_KView4D();
  auto x3flux = x3flux_in.get_KView4D();
  auto u_out = u_out_in.get_KView4D();


  int is = pmb->is; int js = pmb->js; int ks = pmb->ks;
  int ie = pmb->ie; int je = pmb->je; int ke = pmb->ke;

  const Real dx = (pmb->block_size.x1max-pmb->block_size.x1min)/pmb->block_size.nx1;
  const Real dy = (pmb->block_size.x2max-pmb->block_size.x2min)/pmb->block_size.nx2;
  const Real dz = (pmb->block_size.x3max-pmb->block_size.x3min)/pmb->block_size.nx3;

  const Real dt = pmb->pmy_mesh->dt;

  const int nx2 = pmb->block_size.nx2;
  const int nx3 = pmb->block_size.nx3;


  //Add the divergence of the flux to u_out
  athena_for("Add flux divergence",0,NHYDRO-1,ks,ke,js,je,is,ie,
  KOKKOS_LAMBDA (int n, int k, int j, int i) {
      // calculate x1-flux divergence
      Real dflx = (x1flux(n,k,j,i+1) - x1flux(n,k,j,i))/dx;

      // calculate x2-flux divergence
      if (nx2 > 1) {
      dflx += (x2flux(n,k,j+1,i) - x2flux(n,k,j,i))/dy;
      }

      // calculate x3-flux divergence
      if (nx3 > 1) {
      dflx += (x3flux(n,k+1,j,i) - x3flux(n,k,j,i))/dz;
      }

      // update conserved variables
      u_out(n,k,j,i) -= wght*(dt)*dflx;
  });

  // add coordinate (geometric) source terms
  pmb->pcoord->CoordSrcTerms((wght*pmb->pmy_mesh->dt),pmb->phydro->flux,w,bcc,u_out_in);

  return;
}

//----------------------------------------------------------------------------------------
//! \fn  void Hydro::WeightedAveU
//  \brief Compute weighted average of cell-averaged U in time integrator step

void Hydro::WeightedAveU(AthenaArray<Real> &u_out_in, AthenaArray<Real> &u_in1_in,
                         AthenaArray<Real> &u_in2_in, const Real wght[3]) {
  MeshBlock *pmb=pmy_block;
  int is = pmb->is; int js = pmb->js; int ks = pmb->ks;
  int ie = pmb->ie; int je = pmb->je; int ke = pmb->ke;

  auto u_out = u_out_in.get_KView4D();
  auto u_in1 = u_in1_in.get_KView4D();
  auto u_in2 = u_in2_in.get_KView4D();

  const Real wght0 = wght[0];
  const Real wght1 = wght[1];
  const Real wght2 = wght[2];

  // consider every possible simplified form of weighted sum operator:
  // U = a*U + b*U1 + c*U2
  // if c=0, c=b=0, or c=b=a=0 (in that order) to avoid extra FMA operations

  // u_in2 may be an unallocated AthenaArray if using a 2S time integrator
  if (wght[2] != 0.0) {
    athena_for("WeightedAveU wght[2] != 0",0,NHYDRO-1,ks,ke,js,je,is,ie,
    KOKKOS_LAMBDA (int n, int k, int j, int i) {
        u_out(n,k,j,i) = wght0*u_out(n,k,j,i) + wght1*u_in1(n,k,j,i)
            + wght2*u_in2(n,k,j,i);
    });
  } else { // do not dereference u_in2
    if (wght[1] != 0.0) {
      athena_for("WeightedAveU wght[1] != 0",0,NHYDRO-1,ks,ke,js,je,is,ie,
      KOKKOS_LAMBDA (int n, int k, int j, int i) {
          u_out(n,k,j,i) = wght0*u_out(n,k,j,i) + wght1*u_in1(n,k,j,i);
      });
    } else { // do not dereference u_in1
      if (wght[0] != 0.0) {
        athena_for("WeightedAveU wght[0] != 0",0,NHYDRO-1,ks,ke,js,je,is,ie,
        KOKKOS_LAMBDA (int n, int k, int j, int i) {
            u_out(n,k,j,i) = wght0*u_out(n,k,j,i);
        });
      } else { // directly initialize u_out to 0
        athena_for("WeightedAveU u_out to 0",0,NHYDRO-1,ks,ke,js,je,is,ie,
        KOKKOS_LAMBDA (int n, int k, int j, int i) {
                u_out(n,k,j,i) = 0.0;
        });
      }
    }
  }

  return;
}
