//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file turb.cpp
//  \brief Problem generator for turbulence generator
//

// C++ headers
#include <sstream>
#include <cmath>
#include <stdexcept>
#include <ctime>

// Athena++ headers
#include "../athena.hpp"
#include "../globals.hpp"
#include "../athena_arrays.hpp"
#include "../parameter_input.hpp"
#include "../coordinates/coordinates.hpp"
#include "../eos/eos.hpp"
#include "../field/field.hpp"
#include "../hydro/hydro.hpp"
#include "../mesh/mesh.hpp"
#include "../utils/utils.hpp"
#include "../fft/few_modes_turbulence.hpp"

#ifdef OPENMP_PARALLEL
#include <omp.h>
#endif


static Real hst_turbulence(MeshBlock *pmb, int iout) {
  int is=pmb->is, ie=pmb->ie, js=pmb->js, je=pmb->je, ks=pmb->ks, ke=pmb->ke;

  Kokkos::View<Real****, Kokkos::LayoutRight, DevSpace> bcc;
  if (MAGNETIC_FIELDS_ENABLED) {
    bcc = pmb->pfield->bcc.get_KView4D();
  }
  auto w = pmb->phydro->w.get_KView4D();
  Real gam = pmb->peos->GetGamma();

  // after this function is called the result is MPI_SUMed across all procs/meshblocks
  // thus, we're only concerned with local sums
  Real sum;
  Kokkos::Sum<Real> reducer_sum(sum);

  Kokkos::parallel_reduce("Ms",
    Kokkos::MDRangePolicy< Kokkos::Rank<3> >({ks,js,is},{ke+1,je+1,ie+1},{1,1,ie+1-is}),
    KOKKOS_LAMBDA( const int k, const int j, const int i, Real& sum){

    Real vel2 = (w(IVX,k,j,i)*w(IVX,k,j,i) +
                 w(IVY,k,j,i)*w(IVY,k,j,i) +
                 w(IVZ,k,j,i)*w(IVZ,k,j,i));

    Real c_s = std::sqrt(gam * w(IPR,k,j,i)/w(IDN,k,j,i)); // speed of sound

    Real e_kin = 0.5 * w(IDN,k,j,i) * vel2;

    if (iout == 0) { // Ms
      sum += std::sqrt(vel2)/c_s;
    } else if (iout == 2) { // Ekin
      sum += e_kin;
    }

    if (MAGNETIC_FIELDS_ENABLED) {
      Real B2 = (bcc(IB1,k,j,i)*bcc(IB1,k,j,i) +
                 bcc(IB2,k,j,i)*bcc(IB2,k,j,i) +
                 bcc(IB3,k,j,i)*bcc(IB3,k,j,i));

      Real e_mag = 0.5*B2;

      if (iout == 1) { // Ma
        sum += std::sqrt(e_kin/e_mag);
      } else if (iout == 3) { // Emag
        sum += e_mag;
      } else if (iout == 4) { // plasma beta
        sum += w(IPR,k,j,i)/e_mag;
      }
    }
  },reducer_sum);

  // assuming unit box size
  sum /= (pmb->pmy_mesh->mesh_size.nx1 *
          pmb->pmy_mesh->mesh_size.nx2 *
          pmb->pmy_mesh->mesh_size.nx3);

  return sum;
}

//========================================================================================
//! \fn void Mesh::InitUserMeshData(ParameterInput *pin)
//  \brief
//========================================================================================
void Mesh::InitUserMeshData(ParameterInput *pin) {
  fmturb_flag = pin->GetInteger("problem","fmturb_flag");
  AllocateUserHistoryOutput(5);
  EnrollUserHistoryOutput(0, hst_turbulence, "Mean Ms");
  EnrollUserHistoryOutput(1, hst_turbulence, "Mean Ma");
  EnrollUserHistoryOutput(2, hst_turbulence, "Mean Ekin");
  EnrollUserHistoryOutput(3, hst_turbulence, "Mean Emag");
  EnrollUserHistoryOutput(4, hst_turbulence, "Mean p_b");
  return;
}

//========================================================================================
//! \fn void MeshBlock::InitUserMeshBlockData(ParameterInput *pin)
//  \brief Function to initialize problem-specific data in MeshBlock class.  Can also be
//  used to initialize variables which are global to other functions in this file.
//  Called in MeshBlock constructor before ProblemGenerator.
//========================================================================================

void MeshBlock::InitUserMeshBlockData(ParameterInput *pin) {
  AllocateUserOutputVariables(3);
  SetUserOutputVariableName(0, "acceleration_x");
  SetUserOutputVariableName(1, "acceleration_y");
  SetUserOutputVariableName(2, "acceleration_z");

  auto num_modes = pin->GetInteger("problem","num_modes"); // number of wavemodes
  AllocateRealUserMeshBlockDataField(1);
  ruser_meshblock_data[0].NewAthenaArray(3,num_modes,2);
  return;
}

//========================================================================================
//! \fn void MeshBlock::UserWorkBeforeOutput(ParameterInput *pin)
//  \brief Function called before generating output files
//========================================================================================

void MeshBlock::UserWorkBeforeOutput(ParameterInput *pin) {
  pmy_mesh->pfmtrbd->CopyAccelToOutputVars(user_out_var);
  return;
}


//========================================================================================
//! \fn void MeshBlock::ProblemGenerator(ParameterInput *pin)
//  \brief
//========================================================================================

void MeshBlock::ProblemGenerator(ParameterInput *pin) {
  Real gamma = peos->GetGamma();
  Real gm1 = gamma - 1.0;
  Real p0   = pin->GetReal("problem","p0");
  Real rho0   = pin->GetReal("problem","rho0");
  Real x3min = pmy_mesh->mesh_size.x3min;
  Real Lz = pmy_mesh->mesh_size.x3max - pmy_mesh->mesh_size.x3min;
  Real kz = 2.0*PI/Lz;

  auto u = Kokkos::create_mirror_view(phydro->u.get_KView4D());

  if (MAGNETIC_FIELDS_ENABLED) {
    auto b_x1f = Kokkos::create_mirror_view(pfield->b.x1f.get_KView3D());
    auto b_x2f = Kokkos::create_mirror_view(pfield->b.x2f.get_KView3D());
    auto b_x3f = Kokkos::create_mirror_view(pfield->b.x3f.get_KView3D());

    auto x1v = Kokkos::create_mirror_view(pcoord->x1v.get_KView1D());
    auto x2v = Kokkos::create_mirror_view(pcoord->x2v.get_KView1D());
    auto x3v = Kokkos::create_mirror_view(pcoord->x3v.get_KView1D());
    Kokkos::deep_copy(x1v,pcoord->x1v.get_KView1D());
    Kokkos::deep_copy(x2v,pcoord->x2v.get_KView1D());
    Kokkos::deep_copy(x3v,pcoord->x3v.get_KView1D());

    Real b0 = pin->GetReal("problem","b0");
    auto b_config = pin->GetInteger("problem","b_config");
    for (int k=ks; k<=ke; k++) {
    for (int j=js; j<=je; j++) {
    for (int i=is; i<=ie+1; i++) {
      b_x1f(k,j,i) = 0.0;

      if (b_config == 0) { // uniform field
        b_x1f(k,j,i) = b0;
      }
      if (b_config == 1) { // no net flux with uniform fieldi
        if (x3v(k) < x3min + Lz/2.0) {
          b_x1f(k,j,i) = b0;
        } else {
          b_x1f(k,j,i) = -b0;
        }
      }
      if (b_config == 2) { // no net flux with sin(z) shape
        // sqrt(0.5) is used so that resulting e_mag is approx b_0^2/2 similar to
        // other b_configs
        b_x1f(k,j,i) = b0 / std::sqrt(0.5) * sin(kz*x3v(k));
      }

      // setting cell centered energy after the right hand side face has been set
      if (NON_BAROTROPIC_EOS && i > is) {
        u(IEN,k,j,i-1) = 0.5 * SQR( 0.5*(b_x1f(k,j,i-1)+b_x1f(k,j,i)) );
      }
    }}}
    for (int k=ks; k<=ke; k++) {
    for (int j=js; j<=je+1; j++) {
    for (int i=is; i<=ie; i++) {
      b_x2f(k,j,i) = 0.0;
    }}}
    for (int k=ks; k<=ke+1; k++) {
    for (int j=js; j<=je; j++) {
    for (int i=is; i<=ie; i++) {
      b_x3f(k,j,i) = 0.0;
    }}}

    Kokkos::deep_copy(pfield->b.x1f.get_KView3D(),b_x1f);
    Kokkos::deep_copy(pfield->b.x2f.get_KView3D(),b_x2f);
    Kokkos::deep_copy(pfield->b.x3f.get_KView3D(),b_x3f);
  }

  for (int k=ks; k<=ke; k++) {
  for (int j=js; j<=je; j++) {
  for (int i=is; i<=ie; i++) {
    u(IDN,k,j,i) = rho0;

    u(IM1,k,j,i) = 0.0;
    u(IM2,k,j,i) = 0.0;
    u(IM3,k,j,i) = 0.0;

    if (NON_BAROTROPIC_EOS) {
      u(IEN,k,j,i) += p0/gm1;
    }
  }}}
  
  //Copy u back to device memory
  Kokkos::deep_copy(phydro->u.get_KView4D(),u);

}


//========================================================================================
//! \fn void Mesh::UserWorkAfterLoop(ParameterInput *pin)
//  \brief
//========================================================================================

void Mesh::UserWorkAfterLoop(ParameterInput *pin) {

}
