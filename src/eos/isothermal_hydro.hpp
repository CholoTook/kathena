#ifndef EOS_ISOTHERMAL_HYDRO_HPP_
#define EOS_ISOTHERMAL_HYDRO_HPP_
//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details

//----------------------------------------------------------------------------------------
// \!fn Real EquationOfState::SoundSpeed(Real dummy_arg[NHYDRO])
// \brief returns isothermal sound speed
KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real dummy_arg[NHYDRO]) const {
  return iso_sound_speed_;
}

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real[], const Real) {return 0.0;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &,
  const AthenaArray<Real> &, int, int, int, int, int, AthenaArray<Real> &,
  AthenaArray<Real> &) {return;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *)
  {return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
  Real *, Real *) {return;}

#endif // EOS_ISOTHERMAL_HYDRO_HPP_
