#ifndef EOS_ADIABATIC_HYDRO_SR_HPP_
#define EOS_ADIABATIC_HYDRO_SR_HPP_
//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================

KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real[]) const {return 0.0;}

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real[], const Real) const {return 0.0;}

//----------------------------------------------------------------------------------------
// Function for calculating relativistic sound speeds
// Inputs:
//   rho_h: enthalpy per unit volume
//   pgas: gas pressure
//   vx: 3-velocity component v^x
//   gamma_lorentz_sq: Lorentz factor \gamma^2
// Outputs:
//   plambda_plus: value set to most positive wavespeed
//   plambda_minus: value set to most negative wavespeed
// Notes:
//   same function as in adiabatic_hydro_gr.cpp
//   references Mignone & Bodo 2005, MNRAS 364 126 (MB)
KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real rho_h, Real pgas, Real vx, Real gamma_lorentz_sq,
                   Real *plambda_plus, Real *plambda_minus) const {
  const Real gamma_adi = gamma_;
  Real cs_sq = gamma_adi * pgas / rho_h;  // (MB 4)
  Real sigma_s = cs_sq / (gamma_lorentz_sq * (1.0-cs_sq));
  Real relative_speed = std::sqrt(sigma_s * (1.0 + sigma_s - SQR(vx)));
  Real sigma_s_tmp = 1.0/(1.0+sigma_s);
  *plambda_plus = sigma_s_tmp * (vx + relative_speed);  // (MB 23)
  *plambda_minus = sigma_s_tmp * (vx - relative_speed);  // (MB 23)
  return;
}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &,
  const AthenaArray<Real> &, int, int, int, int, int, AthenaArray<Real> &,
  AthenaArray<Real> &) const {return;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *) const
  {return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
  Real *, Real *) const {return;}

#endif // EOS_ADIABATIC_HYDRO_SR_HPP_
