#ifndef EOS_ISOTHERMAL_MHD_HPP_
#define EOS_ISOTHERMAL_MHD_HPP_
//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details

//----------------------------------------------------------------------------------------
// \!fn Real EquationOfState::SoundSpeed(Real prim[NHYDRO])
// \brief returns adiabatic sound speed given vector of primitive variables

KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real prim[NHYDRO]) const {
  return iso_sound_speed_;
}

//----------------------------------------------------------------------------------------

// \!fn Real EquationOfState::FastMagnetosonicSpeed()
// \brief returns fast magnetosonic speed given vector of primitive variables
// Note the formula for (C_f)^2 is positive definite, so this func never returns a NaN

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real prim[(NWAVE)], const Real bx) const {
  Real asq = (iso_sound_speed_*iso_sound_speed_)*prim[IDN];
  Real vaxsq = bx*bx;
  Real ct2 = prim[IBY]*prim[IBY] + prim[IBZ]*prim[IBZ];
  Real qsq = vaxsq + ct2 + asq;
  Real tmp = vaxsq + ct2 - asq;
  return std::sqrt(0.5*(qsq + std::sqrt(tmp*tmp + 4.0*asq*ct2))/prim[IDN]);
}


KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &,
  const AthenaArray<Real> &, int, int, int, int, int, AthenaArray<Real> &,
  AthenaArray<Real> &) {return;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *)
  {return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
  Real *, Real *) {return;}

#endif // EOS_ISOTHERMAL_MHD_HPP_
