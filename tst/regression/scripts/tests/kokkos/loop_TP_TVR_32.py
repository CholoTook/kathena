# Test script for "TP-TVR-32" loop structure.

# Modules
import numpy as np
import sys
import scripts.utils.athena as athena
sys.path.insert(0, '../../vis/python')
import athena_read # noqa


# Prepare Athena++
def prepare(**kwargs):
    athena.configure('b',
                     prob='blast',
                     coord='cartesian',
                     kokkos_loop='TP-TVR',
                     kokkos_vector_length=32,
                     flux='hlld', **kwargs)
    athena.make()


# Run Athena++
def run(**kwargs):
    arguments = [
      'job/problem_id=mhd.hlld.adiabatic.Blast',
      'time/tlim=0.6']
    athena.run('mhd/athinput.blast', arguments)


# Analyze outputs
def analyze():
    headers = ['rho', 'press', 'vel', 'Bcc']
    _, _, _, data_ref = athena_read.vtk(
        'data/mhd.hlld.adiabatic.Blast.block0.out1.00006.vtk')
    _, _, _, data_new = athena_read.vtk(
        'bin/mhd.hlld.adiabatic.Blast.block0.out1.00006.vtk')

    for header in headers:
        try:
            # vtk data is single precision only, thus decimal 7 should suffice
            np.testing.assert_almost_equal(data_new[header], data_ref[header], decimal=7)
        except AssertionError:
            print('!!! ERROR: ' + header + ' arrays are not identical.')
            return False

    return True
